<?php
/**
 * Created by PhpStorm.
 * User: Nándor
 * Date: 2017. 04. 22.
 * Time: 17:15
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity()
 * @ORM\Table(name="Todo")
 *
 * Class Todo
 * @package AppBundle\Entity
 *
 */
class Todo
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var integer
     *
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20, nullable=false)
     *
     * @var string
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $description;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $dueDate;
    /**
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Category")
     * @ORM\JoinColumn(name="categoryId", referencedColumnName="id")
     *
     * @var Category
     */
    private $category;

    /**
     * Todo constructor.
     * @param string $title
     * @param string $description
     * @param \DateTime $dueDate
     * @param Category $category
     */
    public function __construct($title, $description = null, \DateTime $dueDate = null, Category $category)
    {
        $this->title = $title;
        $this->description = $description;
        $this->dueDate = $dueDate;
        $this->category = $category;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return \DateTime
     */
    public function getDueDate(): ?\DateTime
    {
        return $this->dueDate;
    }

    /**
     * @return Category
     */
    public function getCategory(): Category
    {
        return $this->category;
    }

    /**
     * @param Category $category
     * @return Todo
     */
    public function setCategory(Category $category): Todo
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @param int $id
     * @return Todo
     */
    public function setId(int $id): Todo
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param string $title
     * @return Todo
     */
    public function setTitle(string $title): Todo
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @param string $description
     * @return Todo
     */
    public function setDescription(string $description): Todo
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @param \DateTime $dueDate
     * @return Todo
     */
    public function setDueDate(\DateTime $dueDate): Todo
    {
        $this->dueDate = $dueDate;
        return $this;
    }
}