<?php
/**
 * Created by PhpStorm.
 * User: Nándor
 * Date: 2017. 04. 22.
 * Time: 17:15
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity()
 * @ORM\Table(name="Category")
 *
 * Class Category
 * @package AppBundle\Entity
 *
 */
class Category
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var integer
     *
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50, nullable=false)
     *
     * @var string
     */
    private $name;

    /**
     * Category constructor.
     * @param $name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

}