<?php
/**
 * Created by PhpStorm.
 * User: Nándor
 * Date: 2017. 04. 23.
 * Time: 9:50
 */

namespace AppBundle\Validator\Constraints;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class CategoryValidator extends ConstraintValidator
{
    /**
     * @var EntityRepository
     */
    private static $repository;

    /**
     * Category constructor.
     * @param EntityRepository $repository
     */
    public function __construct(EntityRepository $repository = null)
    {
        self::$repository = $repository;
    }

    /**
     * @param string $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if($value === null)
        {
            return;
        }
        /** @var Category $categoryEntity */
        $categoryEntity = self::$repository->findOneBy(["name" => $value]);
        if($categoryEntity === null)
        {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ string }}', $value)
                ->addViolation();
        }
    }

    /**
     * @param EntityRepository $repository
     */
    public static function setRepository(EntityRepository $repository)
    {
        self::$repository = $repository;
    }
}