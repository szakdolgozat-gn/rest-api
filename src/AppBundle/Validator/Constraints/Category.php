<?php
/**
 * Created by PhpStorm.
 * User: Nándor
 * Date: 2017. 04. 23.
 * Time: 9:50
 */

namespace AppBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * Class Category
 * @package AppBundle\Validator\Constraints
 *
 * @Annotation
 */
class Category extends Constraint
{
    public $message = 'No such category like "{{ string }}".';

}