<?php
/**
 * Created by PhpStorm.
 * User: Nándor
 * Date: 2017. 04. 22.
 * Time: 16:03
 */

namespace AppBundle\Service;


use AppBundle\Model\Response\Response;
use Symfony\Component\HttpFoundation as Http;
use JMS\Serializer\Serializer;

class ResponseCreator
{
    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * ResponseCreator constructor.
     * @param Serializer $serializer
     */
    public function __construct(Serializer $serializer)
    {
        $this->serializer = $serializer;
    }

    public function createResponse($message, $statusCode)
    {
        $responseData = new Response($statusCode, $message);
        $responseBody = $this->serializer->serialize($responseData, 'json');

        return new Http\Response(
            $responseBody, $statusCode, ['Content-Type' => 'application/json']
        );
    }
}