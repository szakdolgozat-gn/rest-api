<?php
/**
 * Created by PhpStorm.
 * User: Nándor
 * Date: 2017. 04. 22.
 * Time: 12:40
 */

namespace AppBundle\Service;

use AppBundle\Entity\Category;
use AppBundle\Model\Todo as TodoDto;
use AppBundle\Entity\Todo as TodoEntity;
use Doctrine\ORM\EntityRepository;

class TodoConverter
{
    /**
     * @var EntityRepository
     */
    private $repository;

    /**
     * CategoryValidator constructor.
     * @param EntityRepository $repository
     */
    public function __construct(EntityRepository $repository)
    {
        $this->repository = $repository;
    }

    public function convertDtoToEntity(TodoDto $todoDto) : TodoEntity
    {
        /** @var Category $categoryEntity */
        $categoryEntity = $this->repository->findOneBy(["name" => $todoDto->getCategory()]);
        $entity =  new TodoEntity(
            $todoDto->getTitle(),
            $todoDto->getDescription(),
            $todoDto->getDueDate(),
            $categoryEntity
        );
        return $entity;
    }

    public function convertEntityToDto(TodoEntity $todoEntity) : TodoDto
    {
        return new TodoDto(
            $todoEntity->getId(),
            $todoEntity->getTitle(),
            $todoEntity->getDescription(),
            $todoEntity->getDueDate(),
            $todoEntity->getCategory()->getName()
        );
    }

    public function updateEntityFromDto(TodoEntity $todoEntity, TodoDto $todoDto) : TodoEntity
    {
        if($todoDto->getTitle() !== null)
        {
            $todoEntity->setTitle($todoDto->getTitle());
        }
        if($todoDto->getDescription() !== null)
        {
            $todoEntity->setDescription($todoDto->getDescription());
        }
        if($todoDto->getDueDate() !== null)
        {
            $todoEntity->setDueDate($todoDto->getDueDate());
        }
        if($todoDto->getCategory() !== null)
        {
            /** @var Category $categoryEntity */
            $categoryEntity = $this->repository->findOneBy(["name" => $todoDto->getCategory()]);
            $todoEntity->setCategory($categoryEntity);
        }
        return $todoEntity;
    }
}