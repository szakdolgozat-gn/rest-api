<?php
/**
 * Created by PhpStorm.
 * User: Nándor
 * Date: 2017. 04. 26.
 * Time: 23:26
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Category;
use AppBundle\Entity\Todo;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadTodoData implements FixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 5; $i++)
        {
            $TodoEntity = new Todo(
                "Test-title{$i}",
                "test-descriptiton{$i}",
                new \DateTime('now'),
                $manager->getRepository('AppBundle:Category')->findOneBy(["name" => 'Test'])
            );
            $manager->persist($TodoEntity);
            $manager->flush();
        }
    }
}