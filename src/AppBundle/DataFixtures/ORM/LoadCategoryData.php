<?php
/**
 * Created by PhpStorm.
 * User: Nándor
 * Date: 2017. 04. 22.
 * Time: 20:51
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Category;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadCategoryData implements FixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $categories = ["Default", "Shopping", "Billing", "Test"];

        foreach ($categories as $category)
        {
            $categoryEntity = new Category($category);
            $manager->persist($categoryEntity);
            $manager->flush();
        }
    }
}