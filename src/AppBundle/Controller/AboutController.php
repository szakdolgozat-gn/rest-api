<?php
/**
 * Created by PhpStorm.
 * User: Nándor
 * Date: 2017. 05. 01.
 * Time: 19:48
 */

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;


class AboutController extends Controller
{
    /**
     * @Route("/", name="getAbout")
     * @Method("GET")
     * @param Request $request
     * @return Response
     */
    public function aboutAction(Request $request)
    {
        $data = [
            "environment" => $this->getParameter('environment'),
            "commit_id" => $this->getParameter('commit_id'),
            "version" => $this->getParameter('version'),
            "deploy_date" => $this->getParameter('deploy_date')
        ];
        $serialidData = $this->get('jms_serializer')->serialize($data, 'json');

        $response = new Response($serialidData, Response::HTTP_OK);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

}