<?php
/**
 * Created by PhpStorm.
 * User: Nándor
 * Date: 2017. 04. 22.
 * Time: 18:43
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Model\Response\CreatedResponse;
use AppBundle\Model\Response\ValidationError;
use AppBundle\Model\Response\ValidationErrorResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Model\Todo as TodoDto;
use AppBundle\Entity\Todo as TodoEntity;
use Symfony\Component\Validator\ConstraintViolation;

class RestController extends Controller
{

    /**
     * @Route("/", name="listTodos")
     * @Method("GET")
     * @param Request $request
     * @return Response
     */
    public function listAction(Request $request)
    {

        $this->checkAcceptType($request);

        $repo = $this->get('doctrine')->getRepository('AppBundle:Todo');
        $todoEntities = $repo->findAll();

        $converterService = $this->get('todo_converter');
        $todoDtos =  [];
        foreach ($todoEntities as $entity)
        {
            $todoDtos[] = $converterService->convertEntityToDto($entity);
        }
//        $todoDtos = [new TodoDto(1,'test_title', 'test_description,', new \DateTime('now'),'test')];
        $data = $this->get('jms_serializer')->serialize($todoDtos, 'json');

        return new Response($data, Response::HTTP_OK, ['Content-Type', 'application/json']);
    }

    /**
     * @Route("/{id}", name="getTodo")
     * @Method("GET")
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function getAction(Request $request, $id)
    {
        $repo = $this->get('doctrine')->getRepository('AppBundle:Todo');
        $todoEntity = $repo->find($id);

        if($todoEntity === null)
        {
            return new Response(null, Response::HTTP_NOT_FOUND);
        }

        $todoConverter = $this->get('todo_converter');
        $TodoDto = $todoConverter->convertEntityToDto($todoEntity);
        $data = $this->get('jms_serializer')->serialize($TodoDto, 'json');

        return new Response($data, Response::HTTP_OK, ['Content-Type', 'application/json']);
    }

    /**
     * @Route("/", name="createTodo")
     * @Method("POST")
     * @param Request $request
     * @return Response
     */
    public function createAction(Request $request)
    {
        $this->checkAcceptType($request);
        $this->checkContentType($request);

        $serializer = $this->get('jms_serializer');
        try
        {
            /** @var TodoDto $todoDto */
            $todoDto = $serializer->deserialize($request->getContent(), TodoDto::class, 'json');
        }
        catch (\Exception $e)
        {
            return new Response(null, Response::HTTP_BAD_REQUEST);
        }

        $validator = $this->get('validator');
        $violations = $validator->validate($todoDto, null, ['create', 'default']);
        if($violations->count() > 0)
        {
            return $this->createValidationErrorResponse($violations);
        }

        $converter = $this->get('todo_converter');
        $todoEntity= $converter->convertDtoToEntity($todoDto);

        $em = $this->get('doctrine.orm.entity_manager');
        $em->persist($todoEntity);
        $em->flush();

        $responseBody = $serializer->serialize(new CreatedResponse($todoEntity->getId()), 'json');
        return new Response($responseBody, Response::HTTP_CREATED, ['Content-Type', 'application/json']);

    }

    /**
     * @Route("/{id}", name="changeTodo")
     * @Method(methods={"PUT","PATCH"})
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function updateAction(Request $request, $id)
    {
        $this->checkAcceptType($request);
        $this->checkContentType($request);

        $em = $this->get('doctrine.orm.entity_manager');
        $todoRepo = $em->getRepository('AppBundle:Todo');
        $todoEntity = $todoRepo->find($id);
        if($todoEntity === null)
        {
            return new Response(null, Response::HTTP_NOT_FOUND);
        }

        $serializer = $this->get('jms_serializer');
        try
        {
            /** @var TodoDto $todoDto */
            $todoDto = $serializer->deserialize($request->getContent(), TodoDto::class, 'json');
        }
        catch (\Exception $e)
        {
            return new Response(null, Response::HTTP_BAD_REQUEST);
        }

        $validator = $this->get('validator');
        if($request->getMethod() == "put")
        {
            $violations = $validator->validate($todoDto, null, ['put', 'default']);
        }
        else
        {
            $violations = $validator->validate($todoDto, null, ['patch', 'default']);
        }
        if($violations->count() > 0)
        {
            return $this->createValidationErrorResponse($violations);
        }

        $todoEntity = $this->get('todo_converter')->updateEntityFromDto($todoEntity, $todoDto);

        $em->flush();

        return new Response(null, Response::HTTP_NO_CONTENT, ['Content-Type', 'application/json']);

    }

    /**
     * @Route("/{id}", name="deleteTodo")
     * @Method("DELETE")
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function deleteAction(Request $request, $id)
    {
        $repo = $this->get('doctrine')->getRepository('AppBundle:Todo');
        $todoEntity = $repo->find($id);

        if($todoEntity === null)
        {
            return new Response(null, Response::HTTP_NOT_FOUND);
        }

        $em = $this->get('doctrine.orm.entity_manager');
        $em->remove($todoEntity);
        $em->flush();

        return new Response(null, Response::HTTP_OK, ['Content-Type', 'application/json']);
    }

    /**
     * @param Request $request
     * @return Response
     */
    private function checkAcceptType(Request $request)
    {
        if( !in_array('application/json', $request->getAcceptableContentTypes()))
        {
            $responseCreator = $this->get('response_creator');
            return $responseCreator->createResponse(
                'Client must accept json', Response::HTTP_NOT_ACCEPTABLE
            );
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    private function checkContentType(Request $request)
    {
        if($request->getContentType() !== 'json')
        {
            return new Response(
                null, Response::HTTP_UNSUPPORTED_MEDIA_TYPE
            );
        }
    }

    /**
     * @param $violations
     * @return Response
     */
    private function createValidationErrorResponse($violations): Response
    {
        $validationErrors = [];
        /** @var ConstraintViolation $violation */
        foreach ($violations as $violation)
        {
            $validationErrors[] = new ValidationError($violation->getPropertyPath(), $violation->getMessage());
        }
        $validationErrorResponse = new ValidationErrorResponse($validationErrors);
        $responseBody = $this->get('serializer')->serialize($validationErrorResponse, 'json');
        return new Response($responseBody, $validationErrorResponse->getCode(), ['Content-Type', 'application/json']);
    }

}