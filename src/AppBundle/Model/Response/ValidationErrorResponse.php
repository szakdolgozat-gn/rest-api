<?php
/**
 * Created by PhpStorm.
 * User: Nándor
 * Date: 2017. 04. 22.
 * Time: 15:07
 */

namespace AppBundle\Model\Response;

use JMS\Serializer\Annotation as Serializer;

class ValidationErrorResponse extends Response
{
    /**
     * @Serializer\Type("array<AppBundle\Model\Response\ValidationError>")
     * @var array
     */
    private $errors;

    /**
     * ErrorResponse constructor.
     * @param array $errors
     */
    public function __construct(array $errors)
    {
        parent::__construct(
            \Symfony\Component\HttpFoundation\Response::HTTP_BAD_REQUEST,
            'Invalid request'
        );
        $this->errors = $errors;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}