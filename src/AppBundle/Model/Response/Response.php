<?php
/**
 * Created by PhpStorm.
 * User: Nándor
 * Date: 2017. 04. 22.
 * Time: 15:04
 */

namespace AppBundle\Model\Response;

use JMS\Serializer\Annotation as Serializer;

class Response
{

    /**
     * @Serializer\Type("integer")
     * @var integer
     */
    private $code;

    /**
     * @Serializer\Type("string")
     * @var string
     */
    private $message;

    /**
     * Response constructor.
     * @param int $code
     * @param string $message
     */
    public function __construct($code, $message)
    {
        $this->code = $code;
        $this->message = $message;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }
}