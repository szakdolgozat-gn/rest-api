<?php
/**
 * Created by PhpStorm.
 * User: Nándor
 * Date: 2017. 04. 22.
 * Time: 15:18
 */

namespace AppBundle\Model\Response;

use JMS\Serializer\Annotation as Serializer;

class ValidationError
{
    /**
     * @Serializer\Type("string")
     *
     * @var string
     */
    private $property;

    /**
     * @Serializer\Type("string")
     *
     * @var string
     */
    private $message;

    /**
     * ValidationError constructor.
     * @param string $property
     * @param string $message
     */
    public function __construct($property, $message)
    {
        $this->property = $property;
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getProperty(): string
    {
        return $this->property;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }
}