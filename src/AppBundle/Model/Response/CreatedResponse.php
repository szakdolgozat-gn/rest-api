<?php
/**
 * Created by PhpStorm.
 * User: Nándor
 * Date: 2017. 04. 22.
 * Time: 15:05
 */

namespace AppBundle\Model\Response;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\HttpFoundation as Http;

class CreatedResponse extends Response
{
    /**
     * @Serializer\Type("integer")
     *
     * @var integer
     */
    private $resourceId;

    /**
     * ResourceId constructor.
     * @param int $resourceId
     */
    public function __construct($resourceId)
    {
        parent::__construct(Http\Response::HTTP_CREATED, 'Resource created');
        $this->resourceId = $resourceId;
    }

    /**
     * @return int
     */
    public function getResourceId(): int
    {
        return $this->resourceId;
    }
}