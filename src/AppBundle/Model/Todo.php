<?php
/**
 * Created by PhpStorm.
 * User: Nándor
 * Date: 2017. 04. 22.
 * Time: 17:15
 */

namespace AppBundle\Model;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraints as OwnAssert;


class Todo
{
    /**
     * @Serializer\Type("integer")
     *
     * @var integer
     *
     * @Assert\IsNull(groups={"create","put","patch"})
     */
    private $id;

    /**
     * @Serializer\Type("string")
     *
     * @var string
     *
     * @Assert\NotBlank(groups={"create","put"})
     * @Assert\Length(max="20",groups={"default"})
     */
    private $title;

    /**
     * @Serializer\Type("string")
     *
     * @var string
     * @Assert\Length(max="255",groups={"default"})
     *
     */
    private $description;

    /**
     * @Serializer\Type("DateTime<'Y-m-d H:m:s'>")
     *
     * @var \DateTime
     *
     * @Assert\DateTime(groups={"default"})
     *
     */
    private $dueDate;

    /**
     * @Serializer\Type("string")
     *
     * @var string
     *
     * @Assert\NotBlank(groups={"create","put"})
     * @OwnAssert\Category(groups={"create","put","patch","default"})
     *
     */
    private $category;

    /**
     * Todo constructor.
     * @param int $id
     * @param string $title
     * @param string $description
     * @param \DateTime $dueDate
     * @param string $category
     */
    public function __construct($id = null, $title = null, $description = null, \DateTime $dueDate = null, $category = null)
    {
        $this->id = $id;
        $this->title = $title;
        $this->description = $description;
        $this->dueDate = $dueDate;
        $this->category = $category;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Todo
     */
    public function setId(int $id): Todo
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Todo
     */
    public function setTitle(string $title): Todo
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Todo
     */
    public function setDescription(string $description): Todo
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDueDate(): ?\DateTime
    {
        return $this->dueDate;
    }

    /**
     * @param \DateTime $dueDate
     * @return Todo
     */
    public function setDueDate(\DateTime $dueDate): Todo
    {
        $this->dueDate = $dueDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getCategory(): ?string
    {
        return $this->category;
    }

    /**
     * @param string $category
     * @return Todo
     */
    public function setCategory(string $category): Todo
    {
        $this->category = $category;
        return $this;
    }
}