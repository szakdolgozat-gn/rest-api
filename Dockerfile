FROM registry.gitlab.com/szakdolgozat-gn/php:7.1-fpm
MAINTAINER Nandor Gajdacs <gajdacs.nandor@gmail.com>

ENV APP_HOME /app

ENV SYMFONY_ENV prod

COPY . ${APP_HOME}

COPY build/entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

VOLUME ["$APP_HOME", "$APP_HOME/var/logs", "$APP_HOME/app/config/parameters.yml"]

WORKDIR ${APP_HOME}

ENTRYPOINT ["/entrypoint.sh"]

CMD ["php-fpm"]