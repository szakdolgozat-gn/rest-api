#!/usr/bin/env bash

function checkFileExists {
    if [ ! -e "$1" ]; then
        echo "Error! $1 doesn't exist!"
        exit 1
    fi
}

checkFileExists /app/app/config/parameters.yml

setfacl -R -m u:www-data:rwX -m u:`whoami`:rwX /app/var
setfacl -dR -m u:www-data:rwX -m u:`whoami`:rwX /app/var

if [ "$SYMFONY_ENV" = "prod" ]
    then
    rm -f web/app_*.php
    rm -f web/config.php
fi

if [ "$SYMFONY_ENV" = "dev" ]
    then
    chmod +x /app/ci/wait-for-host.sh
    php bin/console doctrine:database:create --if-not-exists --no-interaction
    php bin/console doctrine:schema:drop --force --no-interaction
    php bin/console doctrine:schema:create --no-interaction
    php bin/console doctrine:schema:validate
    php bin/console doctrine:fixtures:load --no-interaction
fi

if [ "$SYMFONY_ENV" = "test" ]
    then
    chmod +x /app/ci/wait-for-host.sh
    /app/ci/wait-for-host.sh -h mysql -p 3306 -d 30 -t 6
    php bin/console doctrine:database:create --if-not-exists --no-interaction
    php bin/console doctrine:schema:drop --force --no-interaction
    php bin/console doctrine:schema:create --no-interaction
    php bin/console doctrine:schema:validate
    php bin/console doctrine:fixtures:load --no-interaction
    php bin/console cache:clear --env=test
fi

php bin/console cache:clear --env=prod --no-debug

set -xe
if [[ ! -z $@ ]]; then
    exec $@
fi