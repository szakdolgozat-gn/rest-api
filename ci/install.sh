#!/usr/bin/env bash
#!/bin/bash

USAGE="Usage: install.sh [-e|--env dev|prod] [-i|--interactive true|false] [-c|--clear-cache true|false]
\t-e|--env:\t\tThe environment (Default: dev)
\t-i|--interaction:\tIf true composer will be run with interactive mode. (Default: true)
\t-c|--clear-cache:\tIf true cache will be cleared. (Default: true)
"

## Set default argument values
ENV="dev"
INTERACTIVE="true"
CLEAR_CACHE="true"

## Check argument count is even
if [ $(( $# % 2 )) -eq 1 ]; then
    echo -e "$USAGE"
    exit -1
fi

## Collect arguments
while [[ $# -gt 1 ]]; do
	key="$1"

	case $key in
		-e|--env)
			ENV="$2"
			shift # past argument
		;;
		-i|--interactive)
			INTERACTIVE="$2"
			shift # past argument
		;;
		-c|--clear-cache)
			CLEAR_CACHE="$2"
			shift # past argument
		;;
		*)
			echo "Invalid option: $key" # invalid option
			echo -e "$USAGE"
			exit -1
		;;
	esac
	shift # past argument or value
done

## Validate arguments
if [[ "$ENV" != "prod" && "$ENV" != "dev" ]]; then
	echo "Invalid argument value (ENV): $ENV."
	echo -e "$USAGE"
	exit -1
fi
if [[ "$INTERACTIVE" != "true" && "$INTERACTIVE" != "false" ]]; then
	echo "Invalid argument value (INTERACTIVE): $INTERACTIVE."
	echo -e "$USAGE"
	exit -1
fi
if [[ "$CLEAR_CACHE" != "true" && "$CLEAR_CACHE" != "false" ]]; then
	echo "Invalid argument value (CLEAR_CACHE): $CLEAR_CACHE."
	echo -e "$USAGE"
	exit -1
fi

echo "Installing application with settings:"
echo "--------------------------------------"
echo -e "\tENV: $ENV"
echo -e "\tINTERACTIVE: $INTERACTIVE"
echo -e "\tCLEAR_CACHE: $CLEAR_CACHE"

set -xe

# 2. Composer
if [ "$ENV" == "dev" ]; then
    if [ "$INTERACTIVE" == "true" ]; then
        composer install
    else
        composer install --no-interaction
    fi
else
    if [ "$INTERACTIVE" == "true" ]; then
        composer install --no-dev --optimize-autoloader
    else
        composer install --no-interaction --no-dev --optimize-autoloader
    fi
fi

# 2. assets install
    php bin/console assets:install


if [ "$CLEAR_CACHE" == "true" ]; then
    php bin/console cache:clear --no-debug --env=$ENV
fi