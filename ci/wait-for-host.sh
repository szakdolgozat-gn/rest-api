#!/bin/bash

USAGE="Usage: wait-for-host.sh [-h|--hostname] [-p|--port] [-d|--delay 10] [-t|--tries 3]
\t-h|--host:\tThe name of the host
\t-p|--port:\tThe number of the port
\t-d|--delay:\tDelay in seconds between tries (Default: 10)
\t-t|--tries:\tMaximum tries. (Default: 3)
"

HOST=""
PORT=""
DELAY="10"
TRIES="3"

## Check argument count is even
if [ $(( $# % 2 )) -eq 1 ]; then
    echo -e "$USAGE"
    exit -1
fi

## Collect arguments
while [[ $# -gt 1 ]]; do
	key="$1"

	case ${key} in
		-h|--hostname)
			HOST="$2"
			shift # past argument
		;;
		-p|--port)
			PORT="$2"
			shift # past argument
		;;
		-d|--delay)
			DELAY="$2"
			shift # past argument
		;;
		-t|--tries)
			TRIES="$2"
			shift # past argument
		;;
		*)
			echo "Invalid option: $key" # invalid option
			echo -e "$USAGE"
			exit -1
		;;
	esac
	shift # past argument or value
done

## Validate arguments
if [[ -z "$HOST" ]] ; then
    echo "You always need to add the hostname!"
	echo -e "$USAGE"
    exit 1
fi
if [[ -z "$PORT" ]] ; then
    echo "You always need to add the port!"
	echo -e "$USAGE"
    exit 1
fi
if ! [[ "$PORT" =~ ^[0-9]+$ ]] ; then
    echo "Port only can be a number"
	echo -e "$USAGE"
    exit 1
fi
if ! [[ "$DELAY" =~ ^[0-9]+$ ]] ; then
    echo "Delay time only can be a number"
	echo -e "$USAGE"
    exit 1
fi
if ! [[ "$TRIES" =~ ^[0-9]+$ ]] ; then
    echo "Tries only can be a number"
	echo -e "$USAGE"
    exit 1
fi

echo "Waiting for $HOST on $PORT port to start"
TRY_COUNT=0
while ! nc -z ${HOST} ${PORT}; do
    TRY_COUNT=$(($TRY_COUNT + 1))
    if [ "$TRY_COUNT" -gt ${TRIES} ]
    then
        echo "Error: Timeout wating for $HOST on $PORT port to start"
        exit 1
    else
        echo "$HOST can not reach on port $PORT ($TRY_COUNT/$TRIES)"
  fi
  sleep ${DELAY}
done
echo "$HOST host on $PORT port is reached!"