#!/usr/bin/env bash

bash /app/ci/wait-for-host.sh -h localhost -p 9000 -d 15 -t 8
bash /app/ci/wait-for-host.sh -h mysql -p 3306 -d 15 -t 6
bash /app/ci/wait-for-host.sh -h nginx -p 80 -d 15 -t 6

set -xe

export SYMFONY_ENV=test

php bin/symfony_requirements

vendor/bin/phpunit --coverage-text --colors=never -c phpunit.all.xml