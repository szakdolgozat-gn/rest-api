#!/usr/bin/env bash

apt-get update -yqq
apt-get install git unzip wget -yqq
apt-get clean

# Composer install
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer

# Install Xdebug
pecl install xdebug
docker-php-ext-enable xdebug
