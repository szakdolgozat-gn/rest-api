#!/usr/bin/env bash

echo -e "${PROD_CONF}" > parameters.yml

sed -i "s/latest/${CI_COMMIT_TAG}/g" ci/deploy/compose-latest.yml
sed -i "s/commit_id: unknown/commit_id: ${CI_COMMIT_SHA}/g" parameters.yml
sed -i "s/version: unknown/version: ${CI_COMMIT_TAG}/g" parameters.yml
sed -i "s/deploy_date: unknown/deploy_date: $(date +%Y-%m-%d:%H:%M:%S)/g" parameters.yml

scp parameters.yml deploy@${UBUNTU_SERVER}:/opt/rest-api/production/parameters.yml
scp ci/deploy/compose-latest.yml deploy@${UBUNTU_SERVER}:/opt/rest-api/production/compose-latest.yml

ssh deploy@${UBUNTU_SERVER} << EOF
    cd /opt/rest-api/production
    docker login -u gitlab-ci-token -p ${CI_JOB_TOKEN} registry.gitlab.com
    docker-compose -f compose-production.yml down --rmi all
    mv compose-latest.yml compose-production.yml
    docker-compose -f compose-production.yml pull
    docker-compose -f compose-production.yml up -d
EOF