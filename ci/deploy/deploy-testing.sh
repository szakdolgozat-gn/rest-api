#!/usr/bin/env bash

echo -e "${TEST_CONF}" > parameters.yml

sed -i "s/commit_id: unknown/commit_id: ${CI_COMMIT_SHA}/g" parameters.yml
sed -i "s/version: unknown/version: ${CI_COMMIT_TAG}/g" parameters.yml
sed -i "s/deploy_date: unknown/deploy_date: $(date +%Y-%m-%d:%H:%M:%S)/g" parameters.yml

scp parameters.yml deploy@${UBUNTU_SERVER}:/opt/rest-api/testing/parameters.yml
scp ci/deploy/compose-testing.yml deploy@${UBUNTU_SERVER}:/opt/rest-api/testing/compose-testing.yml

ssh deploy@${UBUNTU_SERVER} << EOF
    cd /opt/rest-api/testing
    docker login -u gitlab-ci-token -p ${CI_JOB_TOKEN} registry.gitlab.com
    docker-compose -f compose-testing.yml down
    docker-compose -f compose-testing.yml pull
    docker-compose -f compose-testing.yml up -d
EOF