#!/usr/bin/env bash

echo -e "${STAGING_CONF}" > parameters.yml

sed -i "s/commit_id: unknown/commit_id: ${CI_COMMIT_SHA}/g" parameters.yml
sed -i "s/version: unknown/version: ${CI_COMMIT_TAG}/g" parameters.yml
sed -i "s/deploy_date: unknown/deploy_date: $(date +%Y-%m-%d:%H:%M:%S)/g" parameters.yml

scp parameters.yml deploy@${UBUNTU_SERVER}:/opt/rest-api/staging/parameters.yml
scp ci/deploy/compose-staging.yml deploy@${UBUNTU_SERVER}:/opt/rest-api/staging/compose-staging.yml

ssh deploy@${UBUNTU_SERVER} << EOF
    cd /opt/rest-api/staging
    docker login -u $USER -p $PASSWORD registry.gitlab.com
    docker-compose -f compose-staging.yml down
    docker-compose -f compose-staging.yml pull
    docker-compose -f compose-staging.yml up -d
EOF