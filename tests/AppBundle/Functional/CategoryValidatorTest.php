<?php
/**
 * Created by PhpStorm.
 * User: Nándor
 * Date: 2017. 04. 24.
 * Time: 20:19
 */

namespace AppBundle;


use AppBundle\Model\Todo;
use AppBundle\Validator\Constraints\CategoryValidator;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\ValidatorBuilder;

class CategoryValidatorTest extends KernelTestCase
{
    /**
     * @var ValidatorInterface
     */
    private static $validator;

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        self::$validator = static::$kernel->getContainer()->get('validator');
    }

    public function testTodo_WithNullCategoryAtCreate_ShouldHasNotBlankViolation()
    {

        // Arrange
        $todo = new Todo(null, null, null, null, null);

        // Act
        /** @var ConstraintViolationList $violations */
        $violations = self::$validator->validateProperty($todo, 'category', ['create']);

        // Assert
        $this->assertEquals(1, $violations->count());
        $this->assertEquals('category', $violations[0]->getPropertyPath());
        $this->assertEquals('This value should not be blank.', $violations[0]->getMessage());
    }

    public function testTodo_WithBlankCategoryAtCreate_ShouldHasNotBlankViolation()
    {

        // Arrange
        $todo = new Todo(null, null, null, null, "");

        // Act
        /** @var ConstraintViolationList $violations */
        $violations = self::$validator->validateProperty($todo, 'category', ['create']);

        // Assert
        $this->assertEquals(2, $violations->count());
        $this->assertEquals('category', $violations[0]->getPropertyPath());
        $this->assertEquals('This value should not be blank.', $violations[0]->getMessage());
        $this->assertEquals('No such category like "".', $violations[1]->getMessage());
    }

    public function testTodo_WithInvalidCategoryAtCreate_ShouldHasNotBlankViolation()
    {

        // Arrange
        $todo = new Todo(null, null, null, null, "bad-data");

        // Act
        /** @var ConstraintViolationList $violations */
        $violations = self::$validator->validateProperty($todo, 'category', ['create']);

        // Assert
        $this->assertEquals(1, $violations->count());
        $this->assertEquals('category', $violations[0]->getPropertyPath());
        $this->assertEquals('No such category like "bad-data".', $violations[0]->getMessage());
    }

    public function testTodo_WithGoodCategoryAtCreate_ShouldHasNoViolation()
    {

        // Arrange
        $todo = new Todo(null, null, null, null, "test");

        // Act
        /** @var ConstraintViolationList $violations */
        $violations = self::$validator->validateProperty($todo, 'category', ['create']);

        // Assert
        $this->assertEquals(0, $violations->count());
    }
}
