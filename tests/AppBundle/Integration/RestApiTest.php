<?php
/**
 * Created by PhpStorm.
 * User: Nándor
 * Date: 2017. 04. 26.
 * Time: 22:00
 */

namespace AppBundle\Integration;


use AppBundle\Model\Response\CreatedResponse;
use AppBundle\Model\Response\Response;
use AppBundle\Model\Todo;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerBuilder;
use JMS\SerializerBundle\JMSSerializerBundle;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class RestApiTest extends KernelTestCase
{
    /**
     * @var Serializer
     */
    private static $serializer;

    /**
     * @var Client
     */
    private static $client;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private static $em;

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        self::$serializer = static::$kernel->getContainer()->get('serializer');
        self::$client = new Client();
        self::$em = static::$kernel->getContainer()->get('doctrine')->getManager();
    }

    public function testGET_WithGetAllResource_ShouldGetAllResources()
    {
        // Arrange & Act
        $repo = self::$em->getRepository('AppBundle:Todo');
        $allEntity = $repo->findAll();
        $response = self::$client->request('GET', 'http://nginx/todo/');
        $responseBody = json_decode($response->getBody(), true);

        // Assert
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals(count($responseBody), count($allEntity));
    }

    public function testGET_OnTheFirstResource_ShouldGetTheResource()
    {
        $resourceId = 1;
        $repo = self::$em->getRepository('AppBundle:Todo');
        $todoEntity = $repo->findOneBy(["id" => $resourceId]);

        // Act
        $response = self::$client->request('GET', "http://nginx/todo/{$resourceId}");
        /** @var Todo $todoDto */
        $todoDto = self::$serializer->deserialize($response->getBody(), Todo::class, 'json');

        // Assert
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals($todoEntity->getId(), $todoDto->getId());
        $this->assertEquals($todoEntity->getTitle(), $todoDto->getTitle());
        $this->assertEquals($todoEntity->getDescription(), $todoDto->getDescription());
        $this->assertEquals(
            $todoEntity->getDueDate()->format("Y-m-d H:m:s'"),
            $todoDto->getDueDate()->format("Y-m-d H:m:s'")
        );
        $this->assertEquals($todoEntity->getCategory()->getName(), $todoDto->getCategory());
    }

    public function testPOST_WithValidTodo_ShouldBeCreateTheResource()
    {
        // Arrange
        $todo = new Todo(null, "guzzle-test", "test-desc", new \DateTime(),"Test");
        $requestBody = self::$serializer->serialize($todo, 'json');
        $response = self::$client->request('POST', 'http://nginx/todo/', [ "body" => $requestBody]);
        $responseData = json_decode($response->getBody(), true);
        $resourceId = $responseData['resourceId'];
        $repo = self::$em->getRepository('AppBundle:Todo');
        $todoEntity = $repo->findOneBy(["id" => $resourceId]);


        // Act
        $response = self::$client->request('GET', "http://nginx/todo/{$resourceId}");
        /** @var Todo $todoDto */
        $todoDto = self::$serializer->deserialize($response->getBody(), Todo::class, 'json');

        // Assert
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals($todoEntity->getId(), $todoDto->getId());
        $this->assertEquals($todoEntity->getTitle(), $todoDto->getTitle());
        $this->assertEquals($todoEntity->getDescription(), $todoDto->getDescription());
        $this->assertEquals($todoEntity->getDueDate(), $todoDto->getDueDate());
        $this->assertEquals($todoEntity->getCategory()->getName(), $todoDto->getCategory());
    }

    public function testDELETE_WithCreatedResource_ShouldDeleteTheResource()
    {
        // Arrange
        $todo = new Todo(null, "guzzle-test", "test-desc", new \DateTime(),"Test");
        $requestBody = self::$serializer->serialize($todo, 'json');
        $response = self::$client->request('POST', 'http://nginx/todo/', [ "body" => $requestBody]);
        $responseData = json_decode($response->getBody(), true);
        $resourceId = $responseData['resourceId'];
        $repo = self::$em->getRepository('AppBundle:Todo');

        // Act
        $response = self::$client->request('DELETE', "http://nginx/todo/{$resourceId}");

        // Assert
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertNull($repo->findOneBy(["id" => $resourceId]));
    }

}
