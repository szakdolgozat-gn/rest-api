<?php
/**
 * Created by PhpStorm.
 * User: Nándor
 * Date: 2017. 04. 24.
 * Time: 20:19
 */

namespace AppBundle;


use AppBundle\Model\Todo;
use AppBundle\Validator\Constraints\CategoryValidator;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\ValidatorBuilder;

class TodoValidationTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var ValidatorInterface
     */
    private static $validator;

    static function setUpBeforeClass()
    {
        self::$validator = Validation::createValidatorBuilder()
            ->enableAnnotationMapping()
            ->getValidator()
        ;
    }

    public function testTodo_WithNullId_ShouldHasNoViolation()
    {
        // Arrange
        $todo = new Todo();

        // Act
        /** @var ConstraintViolationList $violations */
        $violations = self::$validator->validateProperty($todo, 'id');

        // Assert
        $this->assertEquals(0, $violations->count());
    }

    public function testTodo_WithNotNullId_ShouldHasBeNullViolation()
    {
        // Arrange
        $todo = new Todo(1);

        // Act
        /** @var ConstraintViolationList $violations */
        $violations = self::$validator->validateProperty($todo, 'id', ["create","put","patch"]);

        // Assert
        $this->assertEquals(1, $violations->count());
        $this->assertEquals('id', $violations[0]->getPropertyPath());
        $this->assertEquals('This value should be null.', $violations[0]->getMessage());
    }

    public function testTodo_WithValidTitle_ShouldHasNoViolation()
    {
        // Arrange
        $todo = new Todo(null, "test");

        // Act
        /** @var ConstraintViolationList $violations */
        $violations = self::$validator->validateProperty($todo, 'title', ["create","put","patch","default"]);

        // Assert
        $this->assertEquals(0, $violations->count());
    }

    public function testTodo_WithNullTitle_ShouldHasNotBlankViolation()
    {
        // Arrange
        $todo = new Todo(null, null);

        // Act
        /** @var ConstraintViolationList $violations */
        $violations = self::$validator->validateProperty($todo, 'title', ["create","put","patch","default"]);

        // Assert
        $this->assertEquals(1, $violations->count());
        $this->assertEquals('title', $violations[0]->getPropertyPath());
        $this->assertEquals('This value should not be blank.', $violations[0]->getMessage());
    }

    public function testTodo_WithBlankTitle_ShouldHasNotBlankViolation()
    {
        // Arrange
        $todo = new Todo(null, "");

        // Act
        /** @var ConstraintViolationList $violations */
        $violations = self::$validator->validateProperty($todo, 'title', ["create","put","patch","default"]);

        // Assert
        $this->assertEquals(1, $violations->count());
        $this->assertEquals('title', $violations[0]->getPropertyPath());
        $this->assertEquals('This value should not be blank.', $violations[0]->getMessage());
    }

    public function testTodo_WithTooLongTitle_ShouldHasTooLongViolation()
    {
        // Arrange
        $todo = new Todo(null, "012345678901234567890123456789");

        // Act
        /** @var ConstraintViolationList $violations */
        $violations = self::$validator->validateProperty($todo, 'title', ["create","put","patch","default"]);

        // Assert
        $this->assertEquals(1, $violations->count());
        $this->assertEquals('title', $violations[0]->getPropertyPath());
        $this->assertEquals('This value is too long. It should have 20 characters or less.', $violations[0]->getMessage());
    }

    public function testTodo_WithNullDescription_ShouldHasNoViolation()
    {
        // Arrange
        $todo = new Todo(null, null, null);

        // Act
        /** @var ConstraintViolationList $violations */
        $violations = self::$validator->validateProperty($todo, 'description', ['default']);

        // Assert
        $this->assertEquals(0, $violations->count());
    }

    public function testTodo_WithBlankDescription_ShouldHasNoViolation()
    {
        // Arrange
        $todo = new Todo(null, null, "");

        // Act
        /** @var ConstraintViolationList $violations */
        $violations = self::$validator->validateProperty($todo, 'description', ['default']);

        // Assert
        $this->assertEquals(0, $violations->count());
    }

    public function testTodo_WithTooLongDescription_ShouldHasTooLongViolation()
    {
        // Arrange
        $description = null;
        for ($i = 0; $i < 256; $i++) {
            $description .= "a";
        }
        $todo = new Todo(null, null, $description);

        // Act
        /** @var ConstraintViolationList $violations */
        $violations = self::$validator->validateProperty($todo, 'description', ['default'] );


        // Assert
        $this->assertEquals(1, $violations->count());
        $this->assertEquals('description', $violations[0]->getPropertyPath());
        $this->assertEquals('This value is too long. It should have 255 characters or less.', $violations[0]->getMessage());
    }

    public function testTodo_WithNullDueDate_ShouldHasNoViolation()
    {
        // Arrange
        $todo = new Todo(null, null, null, null);

        // Act
        /** @var ConstraintViolationList $violations */
        $violations = self::$validator->validateProperty($todo, 'dueDate', ['default']);

        // Assert
        $this->assertEquals(0, $violations->count());
    }

    public function testTodo_WithBlankDueDate_ShouldHasNoViolation()
    {
        // Arrange
        $todo = new Todo(null, null, "", null);

        // Act
        /** @var ConstraintViolationList $violations */
        $violations = self::$validator->validateProperty($todo, 'dueDate', ['default']);

        // Assert
        $this->assertEquals(0, $violations->count());
    }

    public function testTodo_WithDateTimeDueDate_ShouldHasNoViolation()
    {
        // Arrange
        $todo = new Todo(null, null, null, new \DateTime());

        // Act
        /** @var ConstraintViolationList $violations */
        $violations = self::$validator->validateProperty($todo, 'dueDate', ['default']);

        // Assert
        $this->assertEquals(0, $violations->count());
    }

    public function testTodo_WithNullCategoryAtCreate_ShouldHasNotBlankViolation()
    {

        // Arrange
        $todo = new Todo(null, null, null, null, null);
        $mock = $this->getMockBuilder(EntityRepository::class)
            ->disableOriginalConstructor()
            ->getMock()
        ;
        CategoryValidator::setRepository($mock);

        // Act
        /** @var ConstraintViolationList $violations */
        $mock->method('findOneBy')->willReturn("test");
        $violations = self::$validator->validateProperty($todo, 'category', ['create']);

        // Assert
        $this->assertEquals(1, $violations->count());
        $this->assertEquals('category', $violations[0]->getPropertyPath());
        $this->assertEquals('This value should not be blank.', $violations[0]->getMessage());
    }

    public function testTodo_WithBlankCategoryAtCreate_ShouldHasNotBlankViolation()
    {

        // Arrange
        $todo = new Todo(null, null, null, null, "");
        $mock = $this->getMockBuilder(EntityRepository::class)
            ->disableOriginalConstructor()
            ->getMock()
        ;
        CategoryValidator::setRepository($mock);

        // Act
        /** @var ConstraintViolationList $violations */
        $mock->method('findOneBy')->willReturn("test");
        $violations = self::$validator->validateProperty($todo, 'category', ['create']);

        // Assert
        $this->assertEquals(1, $violations->count());
        $this->assertEquals('category', $violations[0]->getPropertyPath());
        $this->assertEquals('This value should not be blank.', $violations[0]->getMessage());
    }

    public function testTodo_WithInvalidCategoryAtCreate_ShouldHasNoSuchCategorykViolation()
    {
        // Arrange
        $todo = new Todo(null, null, null, null, "bad-data");
        $mock = $this->getMockBuilder(EntityRepository::class)
            ->disableOriginalConstructor()
            ->getMock()
        ;
        CategoryValidator::setRepository($mock);

        // Act
        /** @var ConstraintViolationList $violations */
        $mock->method('findOneBy')->willReturn(null);
        $violations = self::$validator->validateProperty($todo, 'category', ['create']);

        // Assert
        $this->assertEquals(1, $violations->count());
        $this->assertEquals('category', $violations[0]->getPropertyPath());
        $this->assertEquals('No such category like "bad-data".', $violations[0]->getMessage());
    }

    public function testTodo_WithGoodCategoryAtCreate_ShouldHasNoViolation()
    {
        // Arrange
        $todo = new Todo(null, null, null, null, "test");
        $mock = $this->getMockBuilder(EntityRepository::class)
            ->disableOriginalConstructor()
            ->getMock()
        ;
        CategoryValidator::setRepository($mock);

        // Act
        /** @var ConstraintViolationList $violations */
        $mock->method('findOneBy')->willReturn('test');
        $violations = self::$validator->validateProperty($todo, 'category', ['create']);

        // Assert
        $this->assertEquals(0, $violations->count());
    }

}
